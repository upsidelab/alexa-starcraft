def build_speechlet_response(title, output, reprompt_text, should_end_session):
    return {
        'outputSpeech': {
            'type': 'PlainText',
            'text': output
        },
        'card': {
            'type': 'Simple',
            'title': "Time Tracker" + title,
            'content': output
        },
        'reprompt': {
            'outputSpeech': {
                'type': 'PlainText',
                'text': reprompt_text
            }
        },
        'shouldEndSession': should_end_session
    }


def build_audio_response(title, voice_number, reprompt_text, should_end_session, custom_output=None):
    if custom_output is not None:
        ssml = custom_output
    else:
        ssml = '<speak><audio src="https://s3-eu-west-1.amazonaws.com/alexa-starcraft/voice/{:02d}.mp3" /></speak>'.format(voice_number)

    return {
        'outputSpeech': {
            'type': 'SSML',
            'ssml': ssml
        },
        'reprompt': {
            'outputSpeech': {
                'type': 'PlainText',
                'text': reprompt_text
            }
        },
        'shouldEndSession': should_end_session
    }


def build_response(session_attributes, speechlet_response):
    return {
        'version': '1.0',
        'sessionAttributes': session_attributes,
        'response': speechlet_response
    }
