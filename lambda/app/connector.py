import boto3
import json
import os

def build_more_infantry(count):
    request = {
        'action': 'build_more_infantry',
        'count': count
    }

    _sqs().send_message(QueueUrl=_queue_url(), MessageBody=json.dumps(request))


def back_off_scvs():
    request = {
        'action': 'back_off_scvs'
    }

    _sqs().send_message(QueueUrl=_queue_url(), MessageBody=json.dumps(request))


def field_summary():
    request = {
        'action': 'field_summary'
    }

    _sqs().send_message(QueueUrl=_queue_url(), MessageBody=json.dumps(request))
    response = _sqs().receive_message(QueueUrl=_response_queue_url(), MaxNumberOfMessages=1, WaitTimeSeconds=1)
    if 'Messages' not in response or len(response['Messages']) == 0:
        return None
    else:
        message = response['Messages'][0]
        receipt_handle = message['ReceiptHandle']

        _sqs().delete_message(QueueUrl=_response_queue_url(), ReceiptHandle=receipt_handle)

        return json.loads(message['Body'])


def _sqs():
    return boto3.client('sqs')


def _queue_url():
    return os.environ['COMMAND_QUEUE_URL']


def _response_queue_url():
    return os.environ['RESPONSE_QUEUE_URL']
