import events

def lambda_handler(event, context):
    if event['session']['new']:
        events.on_session_started({'requestId': event['request']['requestId']})

    if event['request']['type'] == 'LaunchRequest':
        return events.on_launch(event['request'], event['session'])
    elif event['request']['type'] == 'IntentRequest':
        return events.on_intent(event['request'], event['session'], event['context'])
    elif event['request']['type'] == 'SessionEndedRequest':
        return events.on_session_ended(event['request'], event['session'])
