from intents import default, commands

def on_session_started(request):
    pass


def on_launch(request, session):
    return default.get_welcome_response()


def on_intent(request, session, context):
    intent_name = request['intent']['name']

    if intent_name == 'BuildMoreInfantry':
        return commands.build_more_infantry(request)
    elif intent_name == 'BackOffScvs':
        return commands.back_off_scvs(request)
    elif intent_name == 'FieldSummary':
        return commands.field_summary(request)
    elif intent_name == 'AMAZON.FallbackIntent':
        return default.get_fallback_intent_response()
    elif intent_name == "AMAZON.HelpIntent":
        return default.get_welcome_response()
    elif intent_name == "AMAZON.CancelIntent" or intent_name == "AMAZON.StopIntent":
        return default.handle_session_end_request()


def on_session_ended(request, session):
    pass