import alexa
import connector
import random

AFFIRMATIVE_RESPONSES = [
    1, # 'Roger that',
    2, # 'At your command',
    3, # 'Working on it',
    4  # 'Affirmative'
]

BUILD_MORE_INFANTRY_RESPONSES = [
    5, # 'Sending help',
    6  #'I\'m sending one out right now'
]

BACK_OFF_SCVS = [
    7  # 'Evacuating SCVs'
]

FIELD_SUMMARY_NO_IDLE_RESPONSES = [
    8, # 'There are no idle SCVs',
    9  # 'All SCVs are busy right now'
]

FIELD_SUMMARY_IDLE_RESPONSES = [
    10, # '{} SCVs are idle',
    11  # '{} SCVs are waiting for your commands'
]

FIELD_SUMMARY_ERROR_RESPONSES = [
    12, # 'I cannot connect to the base, Marshall',
    13  # 'I\'m trying to reach the base but no luck'
]

def build_more_infantry(request):
    count = request['intent']['slots'].get('count', {}).get('value', 5)

    connector.build_more_infantry(int(count))

    return alexa.build_response({}, alexa.build_audio_response(
        'Roger that', random.choice(AFFIRMATIVE_RESPONSES + BUILD_MORE_INFANTRY_RESPONSES), None, True
    ))


def back_off_scvs(_request):
    connector.back_off_scvs()

    return alexa.build_response({}, alexa.build_audio_response(
        'Evacuating SCVs', random.choice(AFFIRMATIVE_RESPONSES + BACK_OFF_SCVS), None, True
    ))


def field_summary(_request):
    summary = connector.field_summary()

    if summary is None:
        return alexa.build_response({}, alexa.build_audio_response(
            'Idle SCVs', random.choice(FIELD_SUMMARY_ERROR_RESPONSES), None, True
        ))

    if int(summary['idle_scvs']) == 0:
        return alexa.build_response({}, alexa.build_audio_response(
            'Idle SCVs', random.choice(FIELD_SUMMARY_NO_IDLE_RESPONSES), None, True
        ))

    return alexa.build_response({}, alexa.build_audio_response(
        'Idle SCVs', None, None, True, custom_output='<speak>{}<audio src="https://s3-eu-west-1.amazonaws.com/alexa-starcraft/voice/{}.mp3"/></speak>'.format(summary['idle_scvs'], random.choice(FIELD_SUMMARY_IDLE_RESPONSES))
    ))
