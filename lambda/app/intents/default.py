import alexa
import random

WELCOME_RESPONSES = [
    21, # 'Welcome Marshall, my name is Kate Blackwater and I was assigned to work alongside with you. I am here to help you command your army. '\
    22  # 'You can tell me to manage soldiers and workers or ask me about battlefield status.',
    # 'Welcome back Marshall. Ready to get into field again?'
]

FALLBACK_RESPONSES = [
    23, # 'I\'m sorry. I wasn\'t trained to do that.',
    24, # 'Sorry, that wasn\'t included in my training.',
    25  # 'Sorry, I have no experience in that.'
]

def get_welcome_response():
    """ If we wanted to initialize the session to have some attributes we could
    add those here
    """
    card_title = "Welcome"
    speech_output = "Ready for action!"
    # If the user either does not reply to the welcome message or says something
    # that is not understood, they will be prompted again with this text.
    reprompt_text = "Give me a command!"
    should_end_session = False
    return alexa.build_response({}, alexa.build_audio_response(
        card_title, random.choice(WELCOME_RESPONSES), reprompt_text, should_end_session))


def get_fallback_intent_response():
    return alexa.build_response({}, alexa.build_audio_response(
        'I cannot do that', random.choice(FALLBACK_RESPONSES), None, True
    ))


def handle_session_end_request():
    card_title = "Roger that"
    speech_output = "Roger that!"
    # Setting this to true ends the session and exits the skill.
    should_end_session = True
    return alexa.build_response({}, alexa.build_audio_response(
        card_title, speech_output, None, should_end_session))
