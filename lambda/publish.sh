#!/bin/bash

rm app.zip
cd app
zip -X -r ../app.zip * -x .git
cd ../env/lib/python3.6/site-packages
zip -X -r ../../../../app.zip *
cd ../../../..
aws lambda update-function-code --function-name AlexaStarcraft --zip-file fileb://app.zip
