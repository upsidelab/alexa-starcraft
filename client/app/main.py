import assistant
import os
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer

run_game(maps.get("Abyssal Reef LE"), [
    Bot(Race.Terran, assistant.Assistant(os.environ['COMMAND_QUEUE_URL'], os.environ['RESPONSE_QUEUE_URL'])),
    Computer(Race.Zerg, Difficulty.Easy)
], realtime=True)
