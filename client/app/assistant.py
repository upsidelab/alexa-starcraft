import sc2
import boto3
import json
from sc2 import constants

class Assistant(sc2.BotAI):
    def __init__(self, queue_url, response_queue_url):
        super().__init__()
        self._sqs = boto3.client('sqs')
        self._queue_url = queue_url
        self._response_queue_url = response_queue_url

        self.infantry_to_send = 0

    async def on_step(self, iteration):
        command = self._receive_command()
        if command is not None:
            if command['action'] == 'build_more_infantry':
                await self._build_more_infantry(command['count'])
            elif command['action'] == 'back_off_scvs':
                await self._back_off_scvs()
            elif command['action'] == 'field_summary':
                await self._field_summary()

        if self.infantry_to_send > 0:
            await self._send_more_infantry()

    def _receive_command(self):
        response = self._sqs.receive_message(QueueUrl=self._queue_url, MaxNumberOfMessages=1)
        if 'Messages' not in response or len(response['Messages']) == 0:
            return None

        message = response['Messages'][0]
        receipt_handle = message['ReceiptHandle']

        self._sqs.delete_message(QueueUrl=self._queue_url, ReceiptHandle=receipt_handle)

        return json.loads(message['Body'])

    async def _build_more_infantry(self, count):
        print('Building more units {}'.format(count))

        barracks = self.units(constants.BARRACKS).owned
        if not barracks.exists:
            print('No barracks exist')
            return

        self.infantry_to_send = count
        for i in range(count):
            response = await self.do(barracks.random.train(constants.MARINE))
            print(response)

    async def _send_more_infantry(self):
        barracks = self.units(constants.BARRACKS).owned
        if barracks.exists:
            barracks = barracks.first
        else:
            return

        idle_infantry = self.units(constants.MARINE).owned.idle

        if idle_infantry.exists:
            idle_infantry_to_send = idle_infantry.closest_to(barracks)
            target = self.known_enemy_structures.random_or(self.enemy_start_locations[0]).position
            self.infantry_to_send -= 1
            await self.do(idle_infantry_to_send.attack(target))

    async def _back_off_scvs(self):
        start_location = self.start_location
        scvs = self.units(constants.SCV).owned

        for scv in scvs:
            await self.do(scv.move(start_location))

    async def _field_summary(self):
        idle_scvs = self.units(constants.SCV).owned.idle
        response = {
            'idle_scvs': idle_scvs.amount
        }

        self._sqs.send_message(QueueUrl=self._response_queue_url, MessageBody=json.dumps(response))
